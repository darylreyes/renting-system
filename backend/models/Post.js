const mongoose = require('mongoose');

const PostSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  desciptions: {
    type: String,
    required: false
  },
  price: {
    type: Number,
    required: true
  },
  location: mongoose.Schema.Types.Mixed,
  user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  images: [{ type: mongoose.Schema.Types.ObjectId, ref: 'FileUploads' }],
  comments: [mongoose.Schema.Types.Mixed]
});

module.exports = mongoose.model('Post', PostSchema);
