const { Schema, model } = require('mongoose');
const FileUploadSchema = new Schema({
  filename: String,
  mimetype: String,
  path: String
});
module.exports = model('FileUploads', FileUploadSchema);
