const moment = require('moment');
module.exports.ErrorValidation = data => {
  const {
    username,
    password,
    role,
    birthday,
    contactNumber,
    email,
    firstName,
    lastName,
    confirmPassword,
    location = {}
  } = data;
  const errors = {};
  if (!username) errors.username = 'Username is Required';
  if (!firstName) errors.firstName = 'First Name is Required';
  if (!lastName) errors.lastName = 'Last Name is Required';
  if (!contactNumber) errors.contactNumber = 'Contact Number is Required';
  if (!role) errors.role = 'Role is Required';
  if (!location.regions) errors.regions = 'Region is Required';
  if (!location.provinces) errors.provinces = 'Province is Required';
  if (!location.city) errors.city = 'City is Required';
  if (!location.barangay) errors.barangay = 'Barangay is Required';
  if (!location.street) errors.street = 'Street is Required';
  const validBday = moment().diff(birthday, 'years');
  if (!birthday) {
    errors.birthday = 'Birthday is Required';
  } else {
    if (validBday < 18) {
      errors.birthday = 'Age must be 18 above';
    }
  }

  if (!email) {
    errors.email = 'Email is Required';
  } else {
    const regEx = /^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/;
    if (!email.match(regEx)) {
      errors.email = 'Email must be a valid email address';
    }
  }
  if (!password) {
    errors.password = 'Password must not empty';
  } else if (password !== confirmPassword) {
    errors.confirmPassword = 'Password must match to confirm password';
  }

  return Object.keys(errors).length ? errors : false;
};
