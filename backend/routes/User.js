const express = require('express');
const userRouter = express.Router();
const passport = require('passport');
const passportConfig = require('../passport');
const JWT = require('jsonwebtoken');
const User = require('../models/User');
const Todo = require('../models/Todo');
const { ErrorValidation } = require('../utils/ErrorValidation');
const { get } = require('lodash');
const signToken = userId => {
  return JWT.sign(
    {
      iss: 'dryl',
      sub: userId
    },
    'dryl',
    { expiresIn: '1h' }
  );
};

userRouter.post('/register', async (req, res) => {
  try {
    const {
      username,
      password,
      role,
      birthday,
      contactNumber,
      email,
      firstName,
      lastName,
      location
    } = req.body;
    const error = ErrorValidation(req.body);
    if (Object.keys(error).length)
      return res.status(400).json({
        message: { msgBody: 'Missing credentials', msgError: true, error }
      });

    const user = await User.findOne({ $or: [{ username }, { email }] });
    const userNameExist = get(user, 'username', false);
    const emailExist = get(user, 'email', false);
    if (userNameExist)
      return res.status(400).json({
        message: { msgBody: 'Username is already taken', msgError: true }
      });
    if (emailExist)
      return res.status(400).json({
        message: { msgBody: 'Email is already taken', msgError: true }
      });
    await new User({
      username,
      password,
      role,
      birthday,
      contactNumber,
      email,
      firstName,
      lastName,
      location
    }).save();
    res.status(200).json({
      message: { msgBody: 'Account Successfully Created', msgError: false }
    });
  } catch (error) {
    console.log(error);
  }
});

userRouter.post('/login', function (req, res, next) {
  passport.authenticate(
    'local',
    { session: false },
    function (err, user, info) {
      try {
        const error = {};
        if (!req.body.username) error.username = 'Username is Required';
        if (!req.body.password) error.password = 'Password is Required';

        if (!Object.keys(error).length && user) {
          const { _id, username, role } = user;
          const token = signToken(_id);
          res.cookie('access_token', token, { httpOnly: true, sameSite: true });
          res
            .status(200)
            .json({ isAuthenticated: true, user: { username, role } });
        } else {
          res.status(500).json({
            message: {
              msgBody: info.message,
              msgError: true,
              error: error
            }
          });
        }
      } catch (error) {
        res.status(500).json({
          message: {
            msgBody: 'Error has occured',
            msgError: true,
            error: error
          }
        });
      }
    }
  )(req, res, next);
});

userRouter.get(
  '/logout',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    res.clearCookie('access_token');
    res.json({ user: { username: '', role: '' }, success: true });
  }
);
userRouter.post(
  '/todo',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const todo = await new Todo(req.body).save();
      req.user.todos.push(todo);
      req.user.save();
      res.status(200).json({
        message: { msgBody: 'Sucessfully created todo', msgError: false }
      });
    } catch (error) {
      console.log(error);
      res
        .status(500)
        .json({ message: { msgBody: 'Error has occured', msgError: true } });
    }
  }
);
userRouter.get(
  '/todos',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const user = await User.findById({ _id: req.user._id })
        .populate('todos')
        .exec();
      res.status(200).json({ todos: user.todos, authenticated: true });
    } catch (error) {
      console.log(error);
      res
        .status(500)
        .json({ message: { msgBody: 'Error has occured', msgError: true } });
    }
  }
);

userRouter.get(
  '/admin',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    if (req.user.role === 'admin') {
      res
        .status(200)
        .json({ message: { msgBody: 'You are an admin', msgError: false } });
    } else {
      res.status(403).json({
        message: { msgBody: 'You are not an admin, go away', msgError: true }
      });
    }
  }
);

userRouter.get(
  '/authenticated',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const { username, role } = req.user;
    res.status(200).json({ isAuthenticated: true, user: { username, role } });
  }
);

module.exports = userRouter;
