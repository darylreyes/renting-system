const express = require('express');
const mongoose = require('mongoose');
const locationRouter = express.Router();
const Schema = mongoose.Schema;
const RegionSchema = new Schema({}, { strict: false });
const RegionCollection = mongoose.model('regions', RegionSchema);
const ProvinceSchema = new Schema({}, { strict: false });
const ProvinceCollection = mongoose.model('provices', ProvinceSchema);
const CitySchema = new Schema({}, { strict: false });
const CityCollection = mongoose.model('citymunicipalities', CitySchema);
const BarangaySchema = new Schema({}, { strict: false });
const BarangayCollection = mongoose.model('barangays', BarangaySchema);

locationRouter.get('/getRegions', async (req, res) => {
  try {
    const regions = await RegionCollection.find({}).lean();
    const modData = regions.map(data => {
      return { value: data.regCode, label: data.regDesc };
    });
    return res.json({ region: modData });
  } catch (error) {
    console.log(error);
  }
});
locationRouter.post('/getProvinces', async (req, res) => {
  try {
    const provinces = await ProvinceCollection.find({
      regCode: req.body.regCode
    }).lean();
    const modProvinces = provinces.map(data => {
      return { value: data.provCode, label: data.provDesc };
    });
    return res.json({ provinces: modProvinces });
  } catch (error) {
    console.log(error);
  }
});
locationRouter.post('/getCities', async (req, res) => {
  try {
    const city = await CityCollection.find({
      provCode: req.body.provCode
    }).lean();
    const modCity = city.map(data => {
      return { value: data.citymunCode, label: data.citymunDesc };
    });
    return res.json({ cities: modCity });
  } catch (error) {
    console.log(error);
  }
});
locationRouter.post('/getBarangays', async (req, res) => {
  try {
    console.log(req.body.citymunCode);
    const barangay = await BarangayCollection.find({
      citymunCode: req.body.citymunCode
    }).lean();
    const modBarangay = barangay.map(data => {
      return { value: data.brgyCode, label: data.brgyDesc };
    });
    return res.json({ barangays: modBarangay });
  } catch (error) {
    console.log(error);
  }
});

module.exports = locationRouter;
