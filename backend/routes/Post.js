const express = require('express');
const passport = require('passport');

const postRouter = express.Router();
const { get } = require('lodash');
const PostModel = require('../models/Post');
const Post = require('../models/Post');

//restful api
postRouter.post(
  '/addPost',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const error = {};
      const requestData = get(req, 'body', {});
      if (!requestData.price) error.price = 'Price is Required';
      if (!requestData.title) error.title = 'Title is Required';
      if (!requestData.location.regions) error.regions = 'Regions is Required';
      if (!requestData.location.provinces)
        error.provinces = 'Provinces is Required';
      if (!requestData.location.city) error.city = 'City is Required';
      if (!requestData.location.barangay)
        error.barangay = 'Barangay is Required';

      console.log(requestData);
      if (!Object.keys(error).length) {
        const postResult = await new PostModel({
          ...requestData,
          user: req.user._id
        }).save();
        return res.json({ msgErr: false });
      } else {
        return res.json({ msgErr: true, error });
      }
    } catch (error) {
      console.log(error);
    }

    // console.log(req.body);
  }
);
postRouter.get(
  '/getposts/:page',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const limit = 9;
      const page = req.params.page;
      let startIndex = (page - 1) * limit - 1;
      startIndex = startIndex < 0 ? 0 : startIndex;
      const posts = await Post.find({ user: req.user._id })
        .populate('images')
        .limit(limit - 1)
        .skip(startIndex)
        .exec();
      const pageCount = await Post.countDocuments().exec();
      console.log(Math.floor(pageCount / limit) + 1);
      const arrayOfPageNumber = [
        ...Array(Math.floor(pageCount / limit) + 1).keys()
      ].map(x => ++x);
      return res.json({ posts, pageCount: arrayOfPageNumber });
    } catch (error) {
      console.log(error);
    }
    // console.log(req.body);
  }
);
postRouter.get(
  '/getPost/:id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const id = get(req, 'params.id', '');
      const post = await Post.findById(id).populate('images').exec();
      return res.json(post);
    } catch (error) {
      console.log(error);
    }
    // console.log(req.body);
  }
);

module.exports = postRouter;
