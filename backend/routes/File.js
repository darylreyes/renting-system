const express = require('express');
const fileRouter = express.Router();
const FileModel = require('../models/FilesUploads');

const { createWriteStream } = require('fs');
const passport = require('passport');
const storeUpload = async ({ stream, filename, mimetype }) => {
  // const id = shortid.generate();
  const path = `FileUploads/${filename}`;
  return new Promise((resolve, reject) =>
    stream
      .pipe(createWriteStream(path))
      .on('finish', () => resolve({ path, filename, mimetype }))
      .on('error', reject)
  );
};
const processUpload = async upload => {
  const { createReadStream, filename, mimetype } = await upload;
  const stream = createReadStream();
  const file = await storeUpload({ stream, filename, mimetype });
  return file;
};
//restful api

fileRouter.post(
  '/addFile',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const image = req.files.file;
      let result = [];
      let upload = {
        filename: image.name,
        mimetype: image.mimetype,
        path: '/images/' + image.name
      };
      const isImageArray = Array.isArray(image);
      if (isImageArray) {
        for (let i = 0; i < image.length; i++) {
          const imageData = image[i];
          upload.filename = imageData.name;
          upload.mimetype = imageData.mimetype;
          upload.path = '/images/' + imageData.name;
          const fileReturnData = await FileModel.create(upload);
          result.push({ id: fileReturnData._id, path: fileReturnData.path });
          imageData.mv('./images/' + imageData.name);
        }
      } else {
        const fileReturnData = await FileModel.create(upload);
        image.mv('./images/' + image.name);
        result.push({ id: fileReturnData._id, path: fileReturnData.path });
      }
      return res.json(result);
    } catch (error) {
      console.log(error);
    }
  }
);

module.exports = fileRouter;
