const express = require('express');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const fileUpload = require('express-fileupload');
const app = express();
app.use(
  cors({
    origin: 'http://localhost:3000',
    credentials: true
  })
);
app.use(
  fileUpload({
    createParentPath: true
  })
);
app.use('/images', express.static('images'));
app.use(cookieParser());
app.use(express.json());

mongoose.connect(
  'mongodb+srv://daryl:daryl@progpractices-icg0v.mongodb.net/rentingsytem?retryWrites=true&w=majority',
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true
  },
  () => {
    console.log('Mongoose is Connected');
  }
);

// ROUTES
const userRouter = require('./routes/User');
const locationRouter = require('./routes/Location');
const fileRouter = require('./routes/File');
const postRouter = require('./routes/Post');
app.use('/user', userRouter);
app.use('/location', locationRouter);
app.use('/file', fileRouter);
app.use('/post', postRouter);

app.listen(5000, () => {
  console.log('express server started');
});
