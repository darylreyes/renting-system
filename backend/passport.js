const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const JwtStrategy = require('passport-jwt').Strategy;
const User = require('./models/User');

const cookieExtractor = req => {
  let token = null;
  if (req && req.cookies) {
    token = req.cookies['access_token'];
  }
  return token;
};

// this is for authorization
passport.use(
  new JwtStrategy(
    {
      jwtFromRequest: cookieExtractor,
      secretOrKey: 'dryl'
    },
    async (payload, done) => {
      try {
        const user = await User.findById({ _id: payload.sub });
        if (!user) return done('User found', false);
        return done(null, user);
      } catch (error) {
        return done(error, false);
      }
    }
  )
);

// this is for authentication strategy
passport.use(
  new LocalStrategy(async (username, password, done) => {
    try {
      const user = await User.findOne({ username });
      if (!user)
        return done(null, false, { message: 'Invalid username or password.' });
      user.comparePassword(password, done);
    } catch (error) {
      return done(error);
    }
  })
);
