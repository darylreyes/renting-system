import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { AuthContext } from '../../Context/AuthContext';
import { Redirect, Route } from 'react-router';

const UnPrivateRoute = ({ component: Component, roles, ...rest }) => {
  const { isAuthenticated } = useContext(AuthContext);

  return (
    <Route
      {...rest}
      render={props => {
        if (isAuthenticated)
          return (
            <Redirect to={{ pathname: '/', state: { from: props.location } }} />
          );
        return <Component {...props} />;
      }}
    />
  );
};

export default UnPrivateRoute;
