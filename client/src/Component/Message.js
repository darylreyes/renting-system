import React from 'react';
import PropTypes from 'prop-types';

function Message({ message }) {
  const getStyle = message => {
    let baseClass = 'alert ';
    if (message.msgError) baseClass = baseClass + 'alert-danger';
    else baseClass = baseClass + 'alert-primary';
    console.log(baseClass);
    return baseClass + ' text-center';
  };
  return (
    <div className={getStyle(message)} role="alert">
      {message.msgBody}
    </div>
  );
}

Message.propTypes = {
  message: PropTypes.object
};

export default Message;
