// import React, { useContext, useEffect, useState } from 'react';
// import PropTypes from 'prop-types';
// import TodoItem from './TodoItem';
// import { AuthContext } from '../../Context/AuthContext';
// import TodoService from '../../Services/TodoService';
// import Message from '../Message';

// function Todos(props) {
//   const [todo, setTodo] = useState({ name: '' });
//   const [todos, setTodos] = useState([]);
//   const [message, setMessage] = useState(null);
//   const authContext = useContext(AuthContext);
//   useEffect(() => {
//     const getTodos = async () => {
//       const { todos, message } = await TodoService.getTodos();
//       if (message) {
//         authContext.setUser({ username: '', role: '' });
//         authContext.setIsAuthenticated(false);
//         return;
//       }
//       setTodos(todos);
//     };
//     getTodos();
//   }, [message]);

//   const resestForm = () => {
//     setTodo({ name: '' });
//   };
//   const handleSubmit = async e => {
//     e.preventDefault();
//     try {
//       const { message } = await TodoService.postTodo(todo);
//       if (!message.msgError) {
//         setMessage(message);
//       } else if (message.msgBody === 'UnAuthorized') {
//         authContext.setUser({ username: '', role: '' });
//         authContext.setIsAuthenticated(false);
//         setMessage(message);
//       } else {
//         setMessage();
//       }
//       resestForm();
//     } catch (error) {
//       console.log(error);
//     }
//   };
//   const handleChange = e => {
//     setTodo({ name: e.target.value });
//   };

//   return (
//     <div>
//       <ul className="list-group">
//         {todos.map((todo, index) => {
//           return <TodoItem todo={todo} key={index} />;
//         })}
//       </ul>
//       <form onSubmit={handleSubmit}>
//         <label>Enter Todo</label>
//         <input
//           type="text"
//           name="todo"
//           value={todo.name}
//           onChange={handleChange}
//           className="form-control"
//           placeholder="Please Enter Todo"
//         />
//         <button className="btn btn-lg btn-primary btn-block" type="submit">
//           Submit
//         </button>
//       </form>
//       {/* {message ? <Message /> : null} */}
//     </div>
//   );
// }

// Todos.propTypes = {};

// export default Todos;
