import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { get } from 'lodash';
import axiosCaller from '../../utils/axiosCaller';
function PostView({ history, match }) {
  const id = get(match, 'params.id', '');
  console.log(id);
  const [post, setPost] = useState({});
  useEffect(() => {
    const getPost = async () => {
      const res = await axiosCaller('GET', `post/getPost/${id}`);
      setPost(res);
    };
    getPost();
  }, []);
  console.log(post);
  return <div>tests</div>;
}

PostView.propTypes = {};

export default PostView;
