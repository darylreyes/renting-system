import React, { useContext, useState } from 'react';
import PropTypes from 'prop-types';
import axiosCaller from '../../utils/axiosCaller';
import Modal from '../../utils/Modal';
import InputComponent from '../../Lib/InputComponent';
import LocationForm from '../../utils/LocationForm';
import { get } from 'lodash';
import {
  locationInitialData,
  postInitialData
} from '../../utils/IntinialState';

function CreatePost({ handleUpdate }) {
  const [images, setImages] = useState([]);
  const [error, setError] = useState([]);
  const [modal, setModal] = useState(false);
  const [location, setLocation] = useState(locationInitialData);
  const [postData, setPostData] = useState(postInitialData);

  const handleImageChange = async e => {
    const file = e.target.files;
    let formData = new FormData();
    var filesLength = file.length;
    for (var i = 0; i < filesLength; i++) {
      formData.append('file', file[i]);
    }
    const res = await axiosCaller('POST', 'file/addFile', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    });

    setImages(res);
    const imgIds = res.map(data => data.id);
    setPostData({ ...postData, images: imgIds });
    console.log(res);
  };
  const handleChange = e => {
    setPostData({ ...postData, [e.target.name]: e.target.value });
    setError({ ...error, [e.target.name]: null });
  };
  const handleSubmit = async () => {
    const { msgErr, error } = await axiosCaller('POST', 'post/addPost', {
      ...postData,
      location
    });
    if (msgErr) {
      return setError(error);
    }
    setLocation(locationInitialData);
    setPostData(postInitialData);
    setModal(false);
    handleUpdate();
  };
  console.log(error);
  const handleModal = () => {
    setModal(!modal);
  };
  return (
    <div>
      <Modal
        onSave={handleSubmit}
        title={'Post Your Place'}
        handleModal={handleModal}
        modalState={modal}
        buttonName="Create Post"
      >
        <div style={{ backgroundColor: 'red' }}>
          {images.map((data, index) => {
            return (
              <img
                key={index}
                src={`http://localhost:5000${data.path}`}
                className="img-thumbnail w-25 h-25"
                alt={data.filename}
              />
            );
          })}
        </div>
        <div className="input-group">
          <input
            type="file"
            className="form-control"
            onChange={handleImageChange}
            multiple
          />
        </div>
        <InputComponent
          label="Title"
          name="title"
          type="text"
          onChange={handleChange}
          value={postData.title}
          className={`form-control ${error['title'] ? 'is-invalid' : ''} `}
          errorMessage={error['title']}
          col={12}
        />
        <LocationForm
          handleGetData={e => {
            const index = e.nativeEvent.target.selectedIndex;
            const getText = get(e.nativeEvent.target[index], 'text', '');
            setLocation({
              ...location,
              [e.target.name]: {
                value: e.target.value,
                label: getText || e.target.name
              }
            });

            setError({ ...error, [e.target.name]: null });
          }}
          error={error}
          location={location}
        />
        <InputComponent
          label="Descriptions"
          name="desciptions"
          type="text"
          onChange={handleChange}
          value={postData.desciptions}
          className={`form-control ${
            error['desciptions'] ? 'is-invalid' : ''
          } `}
          errorMessage={error['desciptions']}
          col={12}
        />
        <InputComponent
          label="Price"
          name="price"
          type="number"
          onChange={handleChange}
          value={postData.price}
          className={`form-control ${error['price'] ? 'is-invalid' : ''} `}
          errorMessage={error['price']}
          col={12}
        />
      </Modal>
    </div>
  );
}

CreatePost.propTypes = {};

export default CreatePost;
