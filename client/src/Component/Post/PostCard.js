import React from 'react';
import PropTypes from 'prop-types';
import { get } from 'lodash';
import ImageCarousel from '../../utils/ImageCarousel';
function PostCard({ post, history }) {
  const title = get(post, 'title', '');
  const id = get(post, '_id', '');
  const desciptions = get(post, 'desciptions', '');
  const images = get(post, 'images', []);
  const price = get(post, 'price', 0);
  return (
    <div className="col-md-6 col-lg-6 col-sm-12 h-100">
      <div className="card mb-3" style={{ maxWidth: '540px' }}>
        <div className="row g-0">
          <div className="col-md-4">
            <ImageCarousel images={images} id={id} />
          </div>
          <div className="col-md-8">
            <div className="card-body">
              <h5 className="card-title">{title}</h5>
              <p className="card-text">{desciptions}</p>
              <p className="card-text">{price}</p>
              <p className="card-text">
                <small className="text-muted">Last updated 3 mins ago</small>
              </p>
              <button
                type="button"
                className="btn btn-primary float-end"
                onClick={() => history.push(`/post/${id}`)}
              >
                View
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

PostCard.propTypes = {
  post: PropTypes.object
};

export default PostCard;
