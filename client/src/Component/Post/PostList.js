import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import axiosCaller from '../../utils/axiosCaller';
import PostCard from './PostCard';
import { get } from 'lodash';
function PostList({ lastUpdated, history }) {
  const [posts, setPosts] = useState([]);
  const [page, setPage] = useState(1);
  const [pageCount, setPageCount] = useState([]);
  useEffect(() => {
    const getPosts = async () => {
      try {
        const res = await axiosCaller('GET', `post/getposts/${page}`);
        const postData = get(res, 'posts', []);
        const numberOfPage = get(res, 'pageCount', []);
        setPageCount(numberOfPage);
        setPosts(postData);
      } catch (error) {
        console.log(error);
      }
    };
    getPosts();
  }, [lastUpdated, page]);
  return (
    <div className="row h-100">
      {posts.map((post, index) => {
        return <PostCard key={index} post={post} history={history} />;
      })}
      <nav aria-label="Page navigation example">
        <ul className="pagination justify-content-end">
          <li className={`page-item ${page === 1 ? 'disabled' : ''}`}>
            <a
              className="page-link"
              tabIndex="-1"
              onClick={() => setPage(page - 1)}
            >
              Previous
            </a>
          </li>
          {pageCount.map((num, index) => {
            return (
              <li
                key={index}
                className={`page-item ${num === page ? 'active' : ''}`}
              >
                <a className="page-link" onClick={() => setPage(num)}>
                  {num}
                </a>
              </li>
            );
          })}

          <li
            className={`page-item ${
              page === pageCount.length ? 'disabled' : ''
            }`}
          >
            <a className="page-link" onClick={() => setPage(page + 1)}>
              Next
            </a>
          </li>
        </ul>
      </nav>
    </div>
  );
}

PostList.propTypes = {};

export default PostList;
