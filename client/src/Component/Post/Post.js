import React, { useState } from 'react';

import CreatePost from './CreatePost';
import PostList from './PostList';

function Post({ history }) {
  const [lastUpdated, setLastUpdated] = useState(Date.now());
  const handleUpdate = () => {
    setLastUpdated(Date.now());
  };
  return (
    <>
      <CreatePost handleUpdate={handleUpdate} />
      <PostList lastUpdated={lastUpdated} history={history} />
    </>
  );
}

Post.propTypes = {};

export default Post;
