import React, { useContext, useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import AuthService from '../../Services/AuthService';
import Message from '../Message';
import InputComponent from '../../Lib/InputComponent';
import SelectComponent from '../../Lib/SelectComponent';
import LocationForm from '../../utils/LocationForm';
import { get } from 'lodash';
function Register(props) {
  const initialData = {
    username: '',
    password: '',
    role: '',
    birthday: '',
    contactNumber: '',
    email: '',
    firstName: '',
    lastName: '',
    confirmPassword: ''
  };
  const [userData, setUserData] = useState(initialData);
  const [error, setError] = useState({});
  const [message, setMessage] = useState(null);
  const [location, setLocation] = useState({
    regions: '',
    provinces: '',
    city: '',
    barangay: '',
    street: ''
  });

  let timerId = useRef(null);

  useEffect(() => {
    return () => {
      clearTimeout(timerId);
    };
  }, []);
  const handleChange = e => {
    setUserData({ ...userData, [e.target.name]: e.target.value });
    setError({ ...error, [e.target.name]: null });
  };

  const resetForm = () => {
    setUserData(initialData);
  };
  const handleSubmit = async e => {
    try {
      e.preventDefault();
      const { message } = await AuthService.register({
        ...userData,
        location
      });
      if (!message.msgError) {
        timerId = setTimeout(() => {
          props.history.push('/login');
        }, 2000);
        resetForm();
      } else {
        message.error && setError(message.error);
      }
      setMessage(message);
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div>
      <div>
        {message ? <Message message={message} /> : null}
        <h3>Register</h3>
        <form className="row g-3" onSubmit={handleSubmit}>
          <InputComponent
            label="First Name"
            name="firstName"
            type="text"
            onChange={handleChange}
            value={userData.firstName}
            className={`form-control ${
              error['firstName'] ? 'is-invalid' : ''
            } `}
            errorMessage={error['firstName']}
            col={6}
          />
          <InputComponent
            label="Last Name"
            name="lastName"
            type="text"
            onChange={handleChange}
            value={userData.lastName}
            className={`form-control ${error['lastName'] ? 'is-invalid' : ''} `}
            errorMessage={error['lastName']}
            col={6}
          />
          <InputComponent
            label="Username"
            name="username"
            type="text"
            onChange={handleChange}
            value={userData.username}
            className={`form-control ${error['username'] ? 'is-invalid' : ''} `}
            errorMessage={error['username']}
            col={6}
          />
          <InputComponent
            label="Email"
            name="email"
            type="text"
            onChange={handleChange}
            value={userData.email}
            className={`form-control ${error['email'] ? 'is-invalid' : ''} `}
            errorMessage={error['email']}
            col={6}
          />
          <InputComponent
            label="Password"
            name="password"
            type="password"
            onChange={handleChange}
            value={userData.password}
            className={`form-control ${error['password'] ? 'is-invalid' : ''} `}
            errorMessage={error['password']}
            col={6}
          />
          <InputComponent
            label="Confirm Password"
            name="confirmPassword"
            type="password"
            onChange={handleChange}
            value={userData.confirmPassword}
            className={`form-control ${
              error['confirmPassword'] ? 'is-invalid' : ''
            } `}
            errorMessage={error['confirmPassword']}
            col={6}
          />

          <InputComponent
            label="Contact Number"
            name="contactNumber"
            type="text"
            onChange={handleChange}
            value={userData.contactNumber}
            className={`form-control ${
              error['contactNumber'] ? 'is-invalid' : ''
            } `}
            errorMessage={error['contactNumber']}
            col={6}
          />
          <InputComponent
            label="Birthday"
            name="birthday"
            type="date"
            onChange={handleChange}
            value={userData.birthday}
            className={`form-control ${error['birthday'] ? 'is-invalid' : ''} `}
            errorMessage={error['birthday']}
            col={6}
          />
          <SelectComponent
            value={userData.role}
            onChange={handleChange}
            errorMessage={error['role']}
            label="Role"
            name="role"
            className={`form-select ${error['role'] ? 'is-invalid' : ''} `}
            options={[
              { value: 0, label: 'Admin' },
              { value: 1, label: 'User' }
            ]}
            col={6}
          />
          <LocationForm
            handleGetData={e => {
              const index = e.nativeEvent.target.selectedIndex;
              const getText = get(e.nativeEvent.target[index], 'text', '');
              setLocation({
                ...location,
                [e.target.name]: {
                  value: e.target.value,
                  label: getText || e.target.name
                }
              });
              setError({ ...error, [e.target.name]: null });
            }}
            error={error}
          />
          <div className="d-grid gap-2 col-6 mx-auto">
            <button className="btn btn-lg btn-primary m-2" type="submit">
              Register
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

Register.propTypes = {};

export default Register;
