import React, { useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import AuthService from '../../Services/AuthService';
import { AuthContext } from '../../Context/AuthContext';
import Message from '../Message';
import InputComponent from '../../Lib/InputComponent';

function Login(props) {
  const authContext = useContext(AuthContext);
  const [userData, setUser] = useState({ username: '', password: '' });
  const [error, setError] = useState({});
  const [message, setMessage] = useState(null);
  const handleChange = e => {
    setUser({ ...userData, [e.target.name]: e.target.value });
  };
  const handleSubmit = async e => {
    e.preventDefault();
    try {
      const { isAuthenticated, user, message } = await AuthService.login(
        userData
      );
      if (isAuthenticated) {
        authContext.setUser(user);
        authContext.setIsAuthenticated(isAuthenticated);
        setUser({ username: '', password: '' });
        props.history.push('/post');
      } else {
        message && setError(message.error);
      }
      setMessage(message);
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div>
      <div>
        <form onSubmit={handleSubmit}>
          <h3>Please Sign In</h3>
          <InputComponent
            label="Username"
            name="username"
            type="text"
            onChange={handleChange}
            value={userData.username}
            className={`form-control ${error['username'] ? 'is-invalid' : ''} `}
            errorMessage="Username is Required"
          />
          <InputComponent
            label="Password"
            name="password"
            type="password"
            onChange={handleChange}
            value={userData.password}
            className={`form-control ${error['password'] ? 'is-invalid' : ''} `}
            errorMessage="Password is Required"
          />
          <button className="btn btn-lg btn-primary btn-block" type="submit">
            Login
          </button>
        </form>
        {message ? <Message message={message} /> : null}
      </div>
    </div>
  );
}

Login.propTypes = {};

export default Login;
