import React, { createContext, useEffect, useState } from 'react';
import AuthService from '../Services/AuthService';

export const AuthContext = createContext();

export default ({ children }) => {
  const [user, setUser] = useState(null);
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [isLoaded, setIsloaded] = useState(false);

  useEffect(() => {
    const authFunction = async () => {
      const data = await AuthService.isAuthenticated();
      setUser(data.user);
      setIsAuthenticated(data.isAuthenticated);
      setIsloaded(true);
    };
    authFunction();
  }, []);

  return (
    <div>
      {!isLoaded ? (
        <h1>Loading</h1>
      ) : (
        <AuthContext.Provider
          value={{ user, setUser, isAuthenticated, setIsAuthenticated }}
        >
          {children}
        </AuthContext.Provider>
      )}
    </div>
  );
};
