import React, { useState } from 'react';
import PropTypes from 'prop-types';

function Modal({
  children,
  title,
  onSave = () => {},
  buttonName = 'Button',
  handleModal = () => {},
  modalState
}) {
  return (
    <div>
      <button
        type="button"
        className="btn btn-primary"
        data-toggle="modal"
        data-target="#exampleModal"
        onClick={handleModal}
      >
        {buttonName}
      </button>

      <div
        className={`modal fade ${modalState && 'show'}`}
        id="exampleModal"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
        style={{ display: modalState ? 'block' : 'none', paddingRight: '17px' }}
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                {title}
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
                onClick={handleModal}
              ></button>
            </div>
            <div className="modal-body">{children}</div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
                onClick={handleModal}
              >
                Close
              </button>
              <button
                type="button"
                className="btn btn-primary"
                onClick={onSave}
              >
                Save changes
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

Modal.propTypes = {
  children: PropTypes.array,
  title: PropTypes.string,
  onSave: PropTypes.func,
  buttonName: PropTypes.string,
  handleModal: PropTypes.func,
  modalState: PropTypes.bool
};

export default Modal;
