import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import SelectComponent from '../Lib/SelectComponent';
import axiosCaller from './axiosCaller';
import { get } from 'lodash';
import InputComponent from '../Lib/InputComponent';
function LocationForm({
  location,
  handleGetData,
  error,
  regionSize = 12,
  provinceSize = 12,
  citySize = 12,
  barangaySize = 12,
  streetSize = 12
}) {
  const [listOfRegions, setListOfRegions] = useState([]);
  const [selectedRegion, setSelectedRegion] = useState('');
  const [listOfProvinces, setListOfProvinces] = useState([]);
  const [selectedProvince, setSelectedProvince] = useState('');
  const [listOfCity, setListOfCity] = useState([]);
  const [selectedCity, setSelectedCity] = useState('');
  const [listOfBarangay, setListOfBarangay] = useState([]);
  const [selectedBarangay, setSelectedBarangay] = useState('');
  const [street, setStreet] = useState('');
  useEffect(async () => {
    const res = await axiosCaller('GET', 'location/getRegions');
    const regionData = get(res, 'region', []);
    setListOfRegions(regionData);
  }, []);
  const handleChangeRegion = async e => {
    setSelectedRegion(e.target.value);
    const res = await axiosCaller('POST', 'location/getProvinces', {
      regCode: e.target.value
    });
    const provinceData = get(res, 'provinces', []);
    setListOfProvinces(provinceData);
    handleGetData(e);
  };
  const handleChangeProvince = async e => {
    setSelectedProvince(e.target.value);
    const res = await axiosCaller('POST', 'location/getCities', {
      provCode: e.target.value
    });
    const cityData = get(res, 'cities', []);
    setListOfCity(cityData);
    handleGetData(e);
  };
  const handleChangeCity = async e => {
    setSelectedCity(e.target.value);
    const res = await axiosCaller('POST', 'location/getBarangays', {
      citymunCode: e.target.value
    });
    const barangayData = get(res, 'barangays', []);
    setListOfBarangay(barangayData);
    handleGetData(e);
  };
  const handleChangeBarangay = e => {
    setSelectedBarangay(e.target.value);
    handleGetData(e);
  };

  const handleChangeStreet = e => {
    setStreet(e.target.value);
    handleGetData(e);
  };

  return (
    <>
      <SelectComponent
        value={selectedRegion}
        onChange={handleChangeRegion}
        errorMessage={error['regions']}
        label="Regions"
        name="regions"
        className={`form-select ${error['regions'] ? 'is-invalid' : ''} `}
        options={listOfRegions}
        col={regionSize}
      />
      <SelectComponent
        value={selectedProvince}
        onChange={handleChangeProvince}
        errorMessage={error['provinces']}
        label="Provinces"
        name="provinces"
        className={`form-select ${error['provinces'] ? 'is-invalid' : ''} `}
        options={listOfProvinces}
        col={provinceSize}
      />
      <SelectComponent
        value={selectedCity}
        onChange={handleChangeCity}
        errorMessage={error['city']}
        label="City"
        name="city"
        className={`form-select ${error['city'] ? 'is-invalid' : ''} `}
        options={listOfCity}
        col={citySize}
      />
      <SelectComponent
        value={selectedBarangay}
        onChange={handleChangeBarangay}
        errorMessage={error['barangay']}
        label="Barangay"
        name="barangay"
        className={`form-select ${error['barangay'] ? 'is-invalid' : ''} `}
        options={listOfBarangay}
        col={barangaySize}
      />
      <InputComponent
        label="Street"
        name="street"
        type="text"
        onChange={handleChangeStreet}
        value={street}
        className={`form-control ${error['street'] ? 'is-invalid' : ''} `}
        errorMessage={error['street']}
        col={streetSize}
      />
    </>
  );
}

LocationForm.propTypes = {
  handleGetData: PropTypes.func,
  error: PropTypes.array,
  regionSize: PropTypes.number,
  provinceSize: PropTypes.number,
  citySize: PropTypes.number,
  barangaySize: PropTypes.number,
  streetSize: PropTypes.number
};

export default LocationForm;
