import React from 'react';
import PropTypes from 'prop-types';

function ImageCarousel({ images, id }) {
  return (
    <div className="row g-0 h-100">
      <div
        id={`carouselExampleControls${id}`}
        className="carousel slide"
        data-bs-ride="carousel"
      >
        <div className="carousel-inner h-100">
          {images.map((image, index) => {
            return (
              <div
                key={index}
                className={`carousel-item ${!index ? 'active' : ''} `}
              >
                <img
                  src={`http://localhost:5000${image.path}`}
                  className="d-block w-100 img-fluid"
                  style={{ minHeight: '210px', maxHeight: '210px' }}
                />
              </div>
            );
          })}
        </div>
        <button
          className="carousel-control-prev"
          type="button"
          data-bs-target={`#carouselExampleControls${id}`}
          data-bs-slide="prev"
        >
          <span
            className="carousel-control-prev-icon"
            aria-hidden="true"
          ></span>
          <span className="visually-hidden">Previous</span>
        </button>
        <button
          className="carousel-control-next"
          type="button"
          data-bs-target={`#carouselExampleControls${id}`}
          data-bs-slide="next"
        >
          <span
            className="carousel-control-next-icon"
            aria-hidden="true"
          ></span>
          <span className="visually-hidden">Next</span>
        </button>
      </div>
    </div>
  );
}

ImageCarousel.propTypes = {
  image: PropTypes.array
};

export default ImageCarousel;
