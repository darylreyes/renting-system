import React from 'react';
import Navbar from './Component/Navbar';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Home from './Component/Home';

import Todos from './Component/Todos/Todos';
import Admin from './Component/Admin/Admin';
import PrivateRoute from './Component/Hoc/PrivateRoute';
import UnPrivateRoute from './Component/Hoc/UnPrivateRoute';
import Login from './Component/Auth/Login';
import Register from './Component/Auth/Register';
import Post from './Component/Post/Post';
import PostView from './Component/Post/PostView';
const USER = '1';
const ADMIN = '0';
function App() {
  return (
    <Router>
      <Navbar />
      <Route exact path="/" component={Home} />
      <UnPrivateRoute exact path="/login" component={Login} />
      <UnPrivateRoute exact path="/register" component={Register} />
      {/* <PrivateRoute path="/todo" roles={['user', 'admin']} component={Todos} /> */}
      <PrivateRoute
        exact
        path="/posts"
        roles={[USER, ADMIN]}
        component={Post}
      />
      <PrivateRoute
        exact
        path="/post/:id"
        roles={[USER, ADMIN]}
        component={PostView}
      />
      <PrivateRoute path="/admin" roles={[ADMIN]} component={Admin} />
    </Router>
  );
}

export default App;
