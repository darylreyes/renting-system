import axiosCaller from '../utils/axiosCaller';

export default {
  login: async user => {
    try {
      const data = await axiosCaller('POST', 'user/login', user);
      return data;
    } catch (error) {
      console.log(error);
      return {
        isAuthenticated: false,
        user: { username: '', role: '' },
        message: error
      };
    }
  },
  register: async user => {
    try {
      const data = await axiosCaller('POST', 'user/register', user);
      return data;
    } catch (error) {
      console.log(error.message);
      return error;
    }
  },
  logout: async () => {
    const data = await axiosCaller('GET', 'user/logout');
    return data;
  },
  isAuthenticated: async () => {
    try {
      const data = await axiosCaller('GET', 'user/authenticated');
      return data;
    } catch (error) {
      console.log(error);
      return { isAuthenticated: false, user: { username: '', role: '' } };
    }
  }
};
