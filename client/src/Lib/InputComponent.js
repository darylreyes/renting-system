import React from 'react';
import PropTypes from 'prop-types';

function InputComponent({
  value,
  onChange,
  errorMessage,
  label,
  name,
  className,
  type,
  col = 12
}) {
  return (
    <div className={`col-md-${col}`}>
      <label className="form-label">{label}</label>
      <div className="input-group has-validation">
        <input
          value={value}
          type={type}
          name={name}
          onChange={onChange}
          className={className}
          placeholder={`${label}`}
        />
        <div className="invalid-feedback">{errorMessage}</div>
      </div>
    </div>
  );
}

InputComponent.propTypes = {
  value: PropTypes.any,
  onChange: PropTypes.func,
  errorMessage: PropTypes.string,
  label: PropTypes.string,
  name: PropTypes.string,
  className: PropTypes.string,
  type: PropTypes.string,
  col: PropTypes.number
};

export default InputComponent;
