import React from 'react';
import PropTypes from 'prop-types';

function SelectComponent({
  value,
  onChange,
  errorMessage,
  label,
  name,
  className,
  options = [],
  col = 12
}) {
  return (
    <div className={`col-md-${col}`}>
      <label htmlFor="role" className="form-label">
        {label}
      </label>
      <div className="input-group has-validation">
        <select
          className={className}
          name={name}
          value={value}
          onChange={onChange}
          aria-describedby="validationServer04Feedback"
        >
          <option value="">Choose...</option>
          {options.map((data, index) => {
            return (
              <option key={index} value={data.value}>
                {data.label}
              </option>
            );
          })}
        </select>
        <div className="invalid-feedback">{errorMessage}</div>
      </div>
    </div>
  );
}

SelectComponent.propTypes = {};

export default SelectComponent;
